#!/usr/bin/python3
# -*- coding: utf8 -*-

# Copyright © 2016 Ordinosor <https://twitter.com/Ordinosor>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES =============================================================================

from tkinter import* # Importation du module tkinter
import tkinter.messagebox
from calendar import monthcalendar # Instancie un calendrier mensuel
from calendar import Calendar # Instancie un calendrier annuel
from time import localtime # Importation de la date et de l'heure locales
from datetime import time, datetime # Module permettant de manipuler les dates et les durées.
import os
import sys
from PIL import Image, ImageTk    # Module PIL (traitement des images importées)
from contextlib import suppress # Module qui permet d'ignorer une exception
                
#==== CLASSE ==============================================================================================

class Agenda(object) :
    "Instanciation de l'agenda"
    
    def __init__(self, init) :
        "Constructeur"        

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

        self.dir_name = os.path.dirname(sys.argv[0])
        for item in os.listdir(os.path.join(self.dir_name, 'agenda')):
            with open(os.path.join(self.dir_name, 'agenda', item), 'r') as ofile:
                rfile = ofile.readlines()
                rfile = ''.join(rfile).replace('\n', '')
                if len(rfile) == 0 :
                    os.remove(os.path.join(self.dir_name, 'agenda', item))

#----------------------------------------------------------------------------------------------------------

    def pages(self, previous_root=None, d=None, m=None, y=None) :
        "Création des pages"    
        
        if d != None and m != None and y != None:           
            self.d = d
            self.m = m
            self.y = y
 
        if previous_root != None:
            previous_root.destroy()

        self.root = Tk()

        self.root.title('Mon agenda')

        self.frame = Frame(self.root, bg='white')
        self.frame.grid()

        self.buttons = Frame(self.frame, bg='white')
        self.buttons.grid(row=0, column=0, sticky = 'w')
    
        # Widget Frame qui contient la date du jour et deux petits triangles de changement de date :
        self.subframe = Frame(self.frame, bg='white', relief='groove', bd=1, padx=250)
        self.subframe.grid(row=1, column=0, columnspan=2, sticky = 'nsew')
    
        #Création de trois boutons d'en-tête. Les commandes sont configurées plus tard :
        self.button_list = list()
        self.textes = ['Enregistrer', 'Supprimer']
        self.commandos = [self.save, self.erase]
        for i in range(0, len(self.textes)):
            self.button = Button(self.buttons, text = self.textes[i], bg='#003366', fg='white',\
                activebackground='white', activeforeground='#003366', command=self.commandos[i])
            self.button.grid(row=0, column=i)
            self.button_list.append(self.button)

        # Création de l'onglet "rechercher" :
        self.search_labelframe = LabelFrame(self.buttons, bd=1, highlightthickness=0, bg='white')
        self.search_labelframe.grid(row=0, column = 4)

        # Importation de l'image de la loupe :
        self.mon_image = os.path.join(self.dir_name, 'icones', 'loupe.png') # Importation de l'image
        self.image = Image.open(self.mon_image)
        self.size = 32, 32 # Réduction de la taille de l'image
        self.image.thumbnail(self.size)
        self.photo = ImageTk.PhotoImage(self.image, master = self.root)

        # Fonction qui efface le mot "Rechercher" lorsque l'utilisateur clique dans le widget "Entry"
        def delete(event):
            if self.search_entry.get() == "Rechercher":
                self.search_entry.delete(0, 'end')

        # Création du widget "chercher":
        self.search_entry = Entry(self.search_labelframe, fg='#003366', font='Times 12 bold', border=0, highlightthickness=0)
        self.search_entry.grid(row=0, column=0)
        self.search_entry.insert(0, "Rechercher")
        self.search_entry.bind('<Button-1>', delete)
        self.search_entry.bind('<Return>', lambda event : self.search_occurrence(self.search_entry))

        # Création du bouton "loupe" :
        self.search = Button(self.search_labelframe, image=self.photo, bd=0, highlightthickness=0, command = lambda : self.search_occurrence(self.search_entry))
        self.search.grid(row=0, column=1)
        
        # Création du triangle de changement de date (vers le futur):
        #self.next_day = Actions(int(self.d)+1, self.m, self.y) # Création de l'objet "Jour d'après" :
        self.right_triangle=Button(self.subframe,bg='white',fg='#003366',text=u"\u25B6",bd=0,highlightthickness=0,padx=0,pady=0,activebackground='#003366',activeforeground='white', command=lambda:self.next_day(self.page, self.d, self.m, self.y))
        self.right_triangle.grid(row = 0, column = 2, padx = 10, sticky = 'nsew')

        # Date sous la forme dd/mm/yyyy.
        # Quatre possibilités différentes concernant l'adjonction d'un "0" supplémentaire : 
        if int(self.d) < 10 and self.m < 10 :
            self.date = Label(self.subframe, bg = 'white', fg='#003366', bd=0, font = 'Times 18 bold', 
            text = '0' + str(self.d) + '/' + '0' + str(self.m) + '/' + str(self.y))
        elif int(self.d) < 10 :
            self.date = Label(self.subframe, bg = 'white', fg='#003366', bd=0, font = 'Times 18 bold', 
            text = '0' + str(self.d) + '/' + str(self.m) + '/' + str(self.y))
        elif self.m < 10 :
            self.date = Label(self.subframe, bg = 'white', fg='#003366', bd=0, font = 'Times 18 bold', 
            text = str(self.d) + '/' + '0' + str(self.m) + '/' + str(self.y))
        else :
            self.date = Label(self.subframe, bg = 'white', fg='#003366', bd=0, font = 'Times 18 bold', text = str(self.d) + '/' + str(self.m) + '/' + str(self.y))
        self.date.grid(row = 0, column = 1, sticky = 'nsew')

        # Création du triangle de changement de date (vers le passé):
        self.left_triangle=Button(self.subframe, bg='white', fg='#003366', text=u"\u25C0", bd=0, highlightthickness=0, padx=0, pady=0, activebackground='#003366', activeforeground='white', command=lambda:self.previous_day(self.page, self.d, self.m, self.y))
        self.left_triangle.grid(row = 0, column = 0, padx = 10, sticky = 'nsew')
    
        # Création des pages de l'agenda
        self.pages_dict = dict()
        for i in range(0,24): # pour chaque heure de la journée :
            self.page = Frame(self.frame, bg='white') # Création d'un cadre
            self.page.grid(row=i+2, column=0, sticky='nsew')
            if i < 10 : # Création des étiquettes affichant les heures de la journée.
                self.label = Label(self.page, fg = 'white', bg = '#003366', padx=5, pady=2,font = 'bold', text = '  ' + str(i) + u"\u2070\u2070")
            else :
                self.label = Label(self.page, fg = 'white', bg = '#003366', padx=5, pady=2,font = 'bold', text = str(i) + u"\u2070\u2070")
            self.label.grid(row=0, column = 0, sticky='nsew')    
            # Création des entrées pour chaque heure de la journée :
            self.hour = Text(self.page, bg ='white', fg = 'black', font ='Times 14', highlightthickness=0, bd=0, selectbackground='blue', selectforeground='white', wrap='word', width=70, height=1, padx=10, pady=2)
            self.hour.grid(row=0, column=1, sticky='nsew')
            self.pages_dict[i] = [self.label, self.hour]

        show = os.listdir(os.path.join(self.dir_name, 'agenda'))
        for item in show:
            if item.replace('-', '/') == self.date['text']:
                with open (os.path.join(self.dir_name, 'agenda', item), 'r') as ofile:
                    rfile = ofile.readlines()
        
        grandchildren = list()
        for child in self.frame.winfo_children(): # pour chaque heure de la journée :
            for grandchild in child.winfo_children():
                if grandchild.winfo_class() == 'Text':
                    grandchildren.append(grandchild)
                            
        try:
            for item, grandchild in zip(rfile, grandchildren):
                grandchild.insert(1.0, item.strip())                
        except:
            pass            

        self.root.mainloop()
                  

#----------------------------------------------------------------------------------------------------------

    def previous_day(self, previous_page, d, m, y) :
        "Conditions à remplir pour reculer d'un jour"
        
        self.d, self.m, self.y = d-1, m, y
        self.previous_page = previous_page
        if self.d < 1 :
            self.m -=1
            if self.m == 0 :
                self.m = 12
                self.y -= 1
            self.c = monthcalendar(self.y, self.m)
            self.d = max(max(self.c))# "max(max" s'explique par le fait que self.c est une liste de listes.         
      
        self.pages(previous_root=self.root, d=self.d, m=self.m, y=self.y)

#----------------------------------------------------------------------------------------------------------

    def next_day(self, previous_page, d, m, y) :
        "Conditions à remplir pour avancer d'un jour"

        self.d, self.m, self.y = d + 1, m, y
        self.previous_page = previous_page
        self.c = monthcalendar(self.y, self.m) # Mois en cours.
        if self.d > max(max(self.c)): # Si le n° du jour est supérieur au n° du dernier jour du mois 
            self.d = '1' # N° du jour = 1
            self.m += 1
            if self.m > 12 :
                self.m = 1  # m = mois
                self.y += 1 # y = year(année)
        self.pages(previous_root=self.root, d=self.d, m=self.m, y=self.y)    

#----------------------------------------------------------------------------------------------------------
        
    def save(self):
        "Enregistrement d'une nouvelle entrée"

        self.entries = []
        
        for child in self.frame.winfo_children(): # pour chaque heure de la journée :
            for grandchild in child.winfo_children():
                if grandchild.winfo_class() == 'Text':
                    self.entries.append(grandchild.get(1.0, 'end'))
                    if child.focus_get() == grandchild:             
                        grandchild['bg'] = 'white'
                        grandchild['fg'] = 'black'
        with open(os.path.join(self.dir_name, 'agenda', self.date['text'].replace('/', '-')), 'w') as wfile:
            for item in self.entries:
                wfile.write(item)
        tkinter.messagebox.showinfo("Modification", "Modification prise en compte")

#----------------------------------------------------------------------------------------------------------
       
    def validate(self):
        "En cours"
        
        for child in self.frame.winfo_children(): # pour chaque heure de la journée :
            for grandchild in child.winfo_children():
                if grandchild.winfo_class() == 'Text' and child.focus_get() == grandchild:
                    grandchild['bg'] = '#003366'
                    grandchild['fg'] = 'white'
                    with open(os.path.join(self.dir_name, 'agenda', self.date['text'].replace('/', '-')), 'r') as ofile:
                        rfile = ofile.readlines()
                        for i, item in enumerate(rfile):
                            if item.strip() == grandchild.get(1.0, 'end').strip():
                                rfile[i:i+1] = '{}validé\n'.format(item.strip()) 
                        with open(os.path.join(self.dir_name, 'agenda', self.date['text'].replace('/', '-')), 'w') as wfile:
                            for item in rfile:
                                wfile.write(item)

#----------------------------------------------------------------------------------------------------------

    def erase(self):
        "En cours"

        for child in self.frame.winfo_children(): # pour chaque heure de la journée :
            for grandchild in child.winfo_children():
                if grandchild.winfo_class() == 'Text' and child.focus_get() == grandchild:
                    grandchild.delete(1.0, 'end')                    
                    self.save()

#----------------------------------------------------------------------------------------------------------

    def search_occurrence(self, search_entry):
        "Méthode qui cherche une chaîne de caractères dans les fichiers"
        #with suppress(Exception): # suppress permet d'ignorer une exception, en l'occurrence un problème d'encodage utf-8 qui ne gène en rien l'exécution du programme.
        self.search_string = search_entry.get()
        self.search_string = self.search_string.lower() # Toutes les majuscules sont transformées en minuscule
        self.listdir = os.listdir(os.path.join(self.dir_name, 'agenda')) # Liste de tous les fichiers contenus dans la chaîne de caractères passée en argument

        self.in_page = Toplevel(bg = 'white', padx=10, pady=10)
        self.in_page.title('Votre recherche')
        self.in_page.transient(self.root)
        self.label = Label(self.in_page, bg = 'white', text = 'Les pages suivantes'+'\n'+'contiennent votre recherche', fg = 'black', font='Times 18 bold')
        self.label.grid(row=0, column = 0, columnspan = 4, pady = 10)
        i2 = 1 # variable d'incrémentation de la ligne dans le Toplevel 
        i3 = 0 # variable d'incrémentation de la colonne dans le Toplevel
        self.selected_buttons = list()
        for i in range(0, len(self.listdir)):
            self.d = int(self.listdir[i].split('-')[0])
            self.m = int(self.listdir[i].split('-')[1])
            self.y = int(self.listdir[i].split('-')[2])
            with open(os.path.join(self.dir_name, 'agenda', self.listdir[i]), 'r') as ofile:
                rfile = ofile.readlines()
                for j, item in enumerate(rfile):
                    rfile[j:j+1] = item.lower() # Toutes les majuscules sont transformées en minuscule
                    if item.find(self.search_string) != -1 : # On cherche si l'occurrence recherchée se trouve dans le fichier ouvert.
                        init = dict()
                        init = {
                                'root': self.root,
                                'd': self.d,
                                'm': self.m,
                                'y': self.y,
                               }
                        self.agenda = Agenda(init)
                    # Création du bouton ouvrant la page ou se trouve l'occurrence :
                        self.button_page = Button(self.in_page, bg='#003366', fg='white', text=self.listdir[i].replace('-','/'), command=self.agenda.pages)
                        self.button_page.grid(row=i2, column = i3, sticky = 'w')
                        self.selected_buttons.append(self.button_page)
                        i3 += 1 # Incrémentation de la colonne
                        if i3 % 4 == 0:
                            i2 += 1 # Incrémentation de la ligne lorsque le modulo 4 de la colonne est égal à 0.
                            i3 = 0    # Réinitialisation de la colonne à 0 lorsqu'une nouvelle ligne commence.
        if len(self.in_page.winfo_children()) == 1 : # Si le Toplevel ne contient aucun bouton, cela signifie qu'aucune page ne contient l'occurrence recherchée.
            self.label.config(bg='red', fg='white', text='Aucun résultat', padx = 50) # Message sur fond rouge : "Aucun résultat"
            
# ##### MAIN PROGRAMM #####################################################################################

if __name__ == "__main__":

    d = localtime()[2] # Current day
    m = localtime()[1] # Current month    
    y = localtime()[0] # Current year

    init = dict()
    init = {
            'd': d,
            'm': m,
            'y': y,
           }
    agenda = Agenda(init)
    agenda.pages()
