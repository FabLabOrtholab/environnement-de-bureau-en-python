#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import tkinter as tk # Importation du module tkinter
import os
from PIL import Image, ImageTk
import sys
import random
import time
import subprocess
from threading import Thread

#==== CLASSES ET MÉTHODES ====================================================

class Wallpaper(Thread):
    "Instanciation de la barre de tâches"

#-----------------------------------------------------------------------------

    def __init__(self, i, _geometry):
        "Constructeur"

        Thread.__init__(self)
        self._geometry = _geometry
        self.i = i
        print(self.i)

#-----------------------------------------------------------------------------

    def run(self):
        "Instanciation de la barre de tâches"

        self.root = tk.Tk()
        self.dir_name = os.path.dirname(sys.argv[0])
        self.root.overrideredirect(1)
        self.root.geometry("%sx%s+%s-0" % (self._geometry[0], self._geometry[1],
            self._geometry[2]))
        self.label=tk.Label(self.root)
        self.label.pack()
        self.root.lower()
        self.update_app()

    def update_app(self) :
        """Remplacement aléatoire du papier peint de bureau"""

        selected_rep = os.path.join(self.dir_name, 'selected_rep')
        with open(selected_rep, 'r') as ofile:
            rfile = ofile.readline()
        self.images = []
        for item in os.listdir(rfile):
            print(item)
            img_format = ['.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG']
            for format_ in img_format:
                if item.endswith('{}'.format(format_)):
                    self.images.append(item)
        print('self.images =', self.images)
        if self.i == 1 :
            self.r = random.randint(1, len(self.images))
            self.img = Image.open(os.path.join(rfile,
                self.images[self.r-1]))
            image_thread = os.path.join(self.dir_name, 'image_thread')
            with open (image_thread, 'w') as wfile:
                wfile.write(os.path.join(rfile, self.images[self.r-1]))

        else :
            try:
                image_thread = os.path.join(self.dir_name, 'image_thread')
                with open(image_thread, 'r') as ofile:
                    rfile = ofile.readline()
                    self.img = Image.open(rfile)

            except :
                default_wallpaper = os.path.join(self.dir_name, 'default_wallpaper')
                with open(default_wallpaper, 'w') as wfile:
                    wfile.write(os.path.join(self.dir_name, 'icones', 'fedora_wallpaper.jpg'))
                with open(default_wallpaper, 'r') as ofile:
                    rfile = ofile.readline().strip()
                    self.img = Image.open(rfile)

        re_size = int(self._geometry[0]), int(self._geometry[1])
        self.first_img = ImageTk.PhotoImage(self.img.resize(re_size),
            master = self.root)
        self.label.configure(image=self.first_img)

        os.path.join(self.dir_name, 'minutes')
        try:
            with open(os.path.join(self.dir_name, 'minutes'), 'r') as ofile:
                rfile = int(ofile.readline().strip())*60000
        except:
            with open(os.path.join(self.dir_name, 'minutes'), 'w') as wfile:
                wfile.write('1')
            with open(os.path.join(self.dir_name, 'minutes'), 'r') as ofile:
                rfile = int(ofile.readline().strip())*60000

        self.root.after(rfile, self.update_app)
        self.root.mainloop()

# == Main programm ===========================================================

if __name__ == "__main__":

    _screen = [[], []]
    for l in subprocess.check_output(["xrandr"]).decode("utf-8").splitlines():
        if " connected " in l :
            _screen[0].append(l.split()[0])

        elif '*' in l :
            _screen[1].append(l.split()[0])

    #_screen) = [['HDMI-O', 'VGA-0'], ['1920x1080', '1920x1080']]
    list_tup = []
    for i, screen_size in enumerate(_screen[1]):
        screensize = _screen[1][i].split('x')
        list_tup.append((screensize[0], screensize[1]))

    #list_tup = [('1920', '1080'), ('1920', '1080')]
    i=1
    x=0
    _threads = []
    while i <= len(_screen[0]):
        wallpaper = Wallpaper(i, (list_tup[i-1][0], list_tup[i-1][1], x))
        _threads.append(wallpaper)
        x+=int(list_tup[i-1][0])
        i+=1

    for _thread in _threads:
        _thread.start()
        time.sleep(0.1)

