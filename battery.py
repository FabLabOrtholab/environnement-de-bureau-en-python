#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import os
import sys
import datetime
import time
import tkinter as tk
from PIL import Image, ImageTk
import subprocess


class Battery(object):

    def __init__(self):
        "constructeur"

    def battery(self):
        
        print('supi')

        self.dir_name = os.path.dirname(sys.argv[0])

        screens = [[], []]
        for l in subprocess.check_output(["xrandr"]).decode("utf-8").splitlines():
            if " connected " in l :
                screens[0].append(l.split()[0])

            elif '*' in l :
                screens[1].append(l.split()[0])

        # screens = [['HDMI-O', 'VGA-0'], ['1920x1080', '1920x1080']]

        self.root = tk.Tk()
        self.root.overrideredirect(1)
        self.root.geometry('+{}-{}'.format(int(screens[1][0].split('x')[0]) - 380, 0))
        self.frame = tk.Frame(self.root, bd=0, highlightthickness=0, bg='black')
        self.frame.grid()

        imgs = [
                'battery-caution.png', 
                'battery-caution-charging.png', 
                'battery-full.png', 
                'battery-full-charged.png', 
                'battery-full-charging.png', 
                'battery-good.png', 
                'battery-good-charging.png', 
                'battery-low.png',
                'battery-low-charging.png', 
                'battery-missing.png',
                'battery-empty.png'
                ]



        icon_size=25, 25
        self.icons = list()
        for img in imgs: 
            icon = Image.open(os.path.join(self.dir_name, 'icones', img))
            icon.thumbnail(icon_size)
            icon_24 = ImageTk.PhotoImage(icon, master=self.root)
            self.icons.append(icon_24)
        while 1:

            self.s()
    def s(self):
        print("supi")
        value = int()
        with open(os.path.join(self.dir_name, 'battery_state'), 'r') as ofile :
            rfile = ofile.readline()
            rfile = rfile.split()
        for i, item in enumerate(rfile):
            if '%,' in item:
                value = item.replace('%,', '')

        self.battery_button = tk.Button(self.frame, bg='black', bd=0, highlightthickness=0)
        self.battery_button.grid(row=0, column=0, padx=5)


        if int(value) > 96 and 'Charging,' in rfile :
            self.battery_button.config(image=self.icons[4], text='{} %'.format(value), compound='right', fg='white')
        elif int(value) > 96 and 'Discharging,' in rfile :
            self.battery_button.config(image=self.icons[2], text='{} %'.format(value), compound='right', fg='white')                                
        elif int(value) > 50 and int(value) < 96 and 'Charging,' in rfile :
            self.battery_button.config(image=self.icons[6], text='{} %'.format(value), compound='right', fg='white')                     
        elif int(value) > 50 and int(value) < 96 and 'Discharging,' in rfile:
            self.battery_button.config(image=self.icons[5], text='{} %'.format(value), compound='right', fg='white')                     
        elif int(value) > 15 and int(value) < 50 and 'Charging,' in rfile :    
            self.battery_button.config(image=self.icons[8], text='{} %'.format(value), compound='right', fg='white')
        elif int(value) > 15 and int(value) < 50 and 'Discharging,' in rfile :
            self.battery_button.config(image=self.icons[7], text='{} %'.format(value), compound='right', fg='white')
        elif int(value) < 15 and 'Charging,' in rfile :                
            self.battery_button.config(image=self.icons[1], text='{} %'.format(value), compound='right', fg='white')
        elif int(value) < 15 and 'Discharging,' in rfile :
            self.battery_button.config(image=self.icons[0], text='{} %'.format(value), compound='right', fg='white')
        elif int(value) < 5 :
            self.battery_button.config(image=self.icons[10], text='{} %'.format(value), compound='right', fg='white')


        self.root.update()
        time.sleep(1)
            


if __name__ == "__main__":

        battery = Battery()
        battery.battery()

