#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import tkinter as tk # Importation du module tkinter
import tkinter.messagebox as tkmess
import tkinter.filedialog as tkfile
import os
from PIL import Image, ImageTk
import sys
import disconnect
import subprocess
import datetime
import time

#==== CLASSES ET MÉTHODES ====================================================


class Taskbar(object):
    "Instanciation de la barre de tâches"

#-----------------------------------------------------------------------------

    def __init__(self):
        "Constructeur"

#-----------------------------------------------------------------------------

    def taskbar(self):
        "Instanciation de la barre de tâches"

        self.dir_name = os.path.dirname(sys.argv[0])
        self.root = tk.Tk()

        screens = [[], []]
        for l in subprocess.check_output(["xrandr"]).decode("utf-8").splitlines():
            if " connected " in l :
                screens[0].append(l.split()[0])

            elif '*' in l :
                screens[1].append(l.split()[0])

        # screens = [['HDMI-O', 'VGA-0'], ['1920x1080', '1920x1080']]
        self.largeur = str(int(screens[1][0].split('x')[0])*2)

        self.root.geometry('{}x35+35+{}'.format(self.largeur,\
            str(int(screens[1][0].split('x')[1]) - 35)))
        self.root.overrideredirect(1)
        self.frame = tk.Frame(self.root, bg='black', bd=0, height=35,\
            width=self.root.winfo_screenwidth())
        self.frame.grid()

        self.subframe = tk.Frame(self.frame, bg='black', bd=0, highlightthickness=0, height=35,\
            width=int(self.largeur))
        self.subframe.grid(row=0, column=0)
        self.subframe.grid_propagate(0)

        self.action()

    def action(self):
        "Instanciation des onglets des applications ouvertes"

        while 1 :
            self.apps = subprocess.Popen(["wmctrl", "-l"], stdout=subprocess.PIPE)
            with open(os.path.join(self.dir_name, 'states', 'present_state'), 'w') as wfile :
                for line in self.apps.stdout:
                    wfile.write(line.decode('UTF-8'))
            with open(os.path.join(self.dir_name, 'states', 'present_state'), 'r') as ofile:
                present_rfile = ofile.readlines()
            with open(os.path.join(self.dir_name, 'states', 'previous_state'), 'r') as ofile2 :
                previous_rfile = ofile2.readlines()
            for item in present_rfile:
                if 'Menu' in item:
                    present_rfile.remove(item)
            if len(previous_rfile) != len(present_rfile):
                with open(os.path.join(self.dir_name, 'states', 'previous_state'), 'w') as wfile :
                    for line in present_rfile:
                        wfile.write(line)

                for child in self.subframe.winfo_children():
                    if child.winfo_class() == 'Button': # and child['compound'] == 'left':
                        child.destroy()

                self.buttons, self.list_id_app, self.showHideList, self.icons, self.app_names, self.icon_names,\
                    app_icons = [], [], [], [], [], [], []

                os.system('wmctrl -l > {}'.format(os.path.join(self.dir_name, 'states', 'present_state')))

                with open(os.path.join(self.dir_name, 'states', 'present_state'), 'r') as ofile1 :
                    rfile1 = ofile1.readlines()

                names_to_be_changed = ['fichiers', 'session', 'temp.pdf', 'Tilix:', "~" ]
                changed_names = ['thunar', 'applications', 'evince', 'Tilix', 'terminal']
                LO_list = ['Base', 'Calc', 'Draw', 'Impress', 'Main', 'Math', 'Writer']
                for item in LO_list:
                    names_to_be_changed.append(item)
                    changed_names.append('libreoffice-{}'.format(item.lower()))
                for i, file_ in enumerate(rfile1):
                    file_ = file_.split()
                    if file_[3] != 'Accueil' and file_[3] != 'Menu':
                        self.id_app = file_[0]
                        self.list_id_app.append(self.id_app)
                        app_name = file_[len(file_)-1] if file_[len(file_)-1] != os.environ.get('USER') else file_[len(file_)-2]
                        for i, name_to_be_changed in enumerate(names_to_be_changed):
                            if app_name == name_to_be_changed:
                                app_name = changed_names[i].capitalize()
                        self.app_names.append(app_name)

                for app_name in self.app_names:
                    self.icon_names.append(app_name.lower() + '.png')

                for i2, id_app in enumerate(self.list_id_app):
                    icon_size = 20, 20
                    os.path.join(self.dir_name, 'icones', self.icon_names[i2])
                    try:
                        icon = Image.open(os.path.join(self.dir_name, 'icones', self.icon_names[i2]))
                    except:
                        icon = Image.open(os.path.join(self.dir_name, 'icones', 'application-default-icon.png'))
                    icon.thumbnail(icon_size)
                    self.icon_30 = ImageTk.PhotoImage(icon, master = self.root)
                    self.icons.append(self.icon_30)
                    self.button = tk.Button(self.subframe, bd=0, bg='black', fg='white',\
                        highlightthickness=0, text=self.app_names[i2], image=self.icon_30, compound='left',\
                        font='Bold 10', pady=10)
                    self.buttons.append(self.button)
                    init = dict()
                    init = {'button': self.button,
                            'id_app': id_app
                           }
                    self.show_or_hide = ShowHide(init)
                    self.showHideList.append(self.show_or_hide)
                for i3, show_or_hide in enumerate(self.showHideList):
                    self.buttons[i3].config(command=show_or_hide.hide)
                    self.buttons[i3].grid(row=0, column=i3+1)
                    self.buttons[i3].bind('<Button-3>', show_or_hide.close_app)
                self.root.after(1000, self.action)

            else:

                self.root.update()



#=============================================================================

class ShowHide(object):
    """Iconification ou déiconofication de la fenêtre sélectionnée"""

#-----------------------------------------------------------------------------

    def __init__(self, init):
        "Constructeur"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#-----------------------------------------------------------------------------

    def hide(self):
        "Iconification de la fenêtre sélectionnée"

        os.system("wmctrl -i -r {} -b add,hidden".format(self.id_app))
        self.button.config(command=self.show)

#-----------------------------------------------------------------------------

    def show(self):
        "Déiconofication de la fenêtre sélectionnée"

        os.system("wmctrl -i -a {}".format(self.id_app))
        self.button.config(command=self.hide)

    def close_app(self, event):
        "Ferme la fenêtre sélectionnée grâcieusement"

        os.system("wmctrl -i -c {}".format(self.id_app))

#=============================================================================

if __name__ == "__main__":
    """Création de l'objet 'taskbar'
    Appel de la méthode taskbar() sur l'objet 'taskbar'"""

    taskbar = Taskbar()
    taskbar.taskbar()

