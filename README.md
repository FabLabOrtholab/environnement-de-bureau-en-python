## Bonjour et bienvenue dans le programme "Un environnement de bureau en Python"

 - Le programme se lance en rajoutant le fichier **launcher.py** dans le fichier caché **~/.config/openbox/autostart**.

 - Il est nécessaire d'installer au préalable, **python3**, **python3-pil.imagetk** et **wmctrl**.

- Vous trouverez plus d'explications sur mon site [miamondo.org](https://miamondo.org/2019/01/08/un-environnement-de-bureau-tout-en-python-1ere-partie/)
