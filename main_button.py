#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import os
import sys
import tkinter as tk
from PIL import Image, ImageTk
import menu_principal

#=============================================================================

class MenuButton(object):
    """Création du bouton du menu principal"""

#-----------------------------------------------------------------------------

    def __init__(self):
        "constructeur"

        self.dir_name = os.path.dirname(sys.argv[0])

#-----------------------------------------------------------------------------

    def menubutton(self):
        """Création du bouton"""

        self.root = tk.Tk()
        self.root.overrideredirect(1)
        self.root.geometry("+{}-{}".format(0, 0))
        self.root.title('Accueil')

        icon_size = 30, 30
        icon = Image.open(os.path.join(self.dir_name, 'icones', 'desktop.png'))
        icon.thumbnail(icon_size)
        self.main_icon = ImageTk.PhotoImage(icon, master = self.root)

        self.menubutton = tk.Button(self.root, width=30, height=30, bd=0,\
            image=self.main_icon)
        self.menubutton.grid(row=0, column=0, sticky='w')

        self.root.lift()

        init = {'dir_name': self.dir_name,
                'menubutton': self.menubutton,
                'wallpaper_label': None,
                'x': None,
                'y': None
                }

        self.commando = menu_principal.MenuPrincipal(init)
        self.menubutton['command'] = self.commando.menu_principal

        self.root.mainloop()

# == Main programm ===========================================================

if __name__ == "__main__":
    """Création de l'objet 'menu_button'
    Appel de la méthode menu_button() sur l'objet 'menu_button'"""

    menu_button = MenuButton()
    menu_button.menubutton()

