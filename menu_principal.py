#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import tkinter as tk
import tkinter.messagebox as tkmess
import os
import sys
from PIL import Image, ImageTk
import disconnect
import wallpaper
import wallpaper2

#=============================================================================

class MenuPrincipal(object):
    """Instanciation du menu principal contenant les applications"""

#-----------------------------------------------------------------------------

    def __init__(self, init):
        "Constructeur"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#-----------------------------------------------------------------------------

    def menu_principal(self, x=None, y=None, wallpaper_label=None):
        "Instanciation des fenêtres et traitement des images"

        if x != None and y != None:
            self.x, self.y = x, y
        if wallpaper_label != None:
            self.wallpaper_label = wallpaper_label

        sys_info = os.uname()

        if sys_info.sysname == "OpenBSD":
            self.dir_app = "/usr/local/share/applications"
        else:
            self.dir_app = "/usr/share/applications"

        self.root = tk.Tk()
        self.root.attributes('-type', 'splash')
        self.root.title('Menu')

        if self.menubutton != None:
            self.root.geometry("+%d-%d" % (0, 35))
        elif self.wallpaper_label != None:
            self.root.geometry("+{}+{}".format(self.x, self.y))

        self.label = tk.Label(self.root, bg='white', bd=2, relief='ridge')
        self.label.grid()

        internet, multimedia, bureautique, development, graphisme,\
            accessoires, network, system = (None,) * 8

        if self.wallpaper_label != None:
            self.wallpaper_label.bind('<Button-3>', self.destroy_root)

        self.shut_down = disconnect.Exit()


        icon_exec_name = [
                          [
                            'terminal.png',
                            'gnome-terminal', 
                            'Terminal'
                          ],
                          [
                            'thunar.png',
                            'thunar', 
                            'Fichiers'
                          ],
                          [
                            'web-browser.png',
                            internet, 
                            'Internet'
                          ],
                          [
                            'application-default-icon.png',
                            'thunar {}'.format(self.dir_app),
                            'Applications'
                          ],
                          [
                            'library-internet-radio.png', 
                            multimedia,
                            'Multimédia'
                          ],
                          [
                            'bureautique.png',
                            bureautique,
                            'Bureautique'
                          ],
                          [
                            'development.png',
                            development,
                            'Développement'
                          ],
                          [
                            'mypaint.png',
                            graphisme,
                            'Graphisme'
                          ],
                          [
                            'accessoires.png',
                            accessoires,
                            'Accessoires'
                          ],
                          [
                            'preferences-system-network.png',
                            network,
                            'Réseau'
                          ],
                          [
                            'system.png',
                            system,
                            'Système'
                          ],
                          [
                            'ejecter.png',
                            self.shut_down.exit,
                            'Déconnexion'
                          ]
                        ]

        j, k = 0, 0 # Variables de positionnement des icônes
        icon_size = 24, 24

        # Traitement des images:
        for item in icon_exec_name:
            self.app_command = item[1]
            self.icon = Image.open(os.path.join(self.dir_name,
                'icones', item[0]))
            self.icon.thumbnail(icon_size)
            img2 = ImageTk.PhotoImage(self.icon, master = self.root)

            if j % 3 == 0 :
                k += 1
                j = 0
            j+=1

            init = dict()
            init =  {
                    'app_command': self.app_command,
                    'dir_app': self.dir_app,
                    'dir_name': self.dir_name,
                    'img': item[0],
                    'icon':self.icon,
                    'app_title': item[2],
                    'img2': img2,
                    'j': j,
                    'k': k,
                    'label': self.label,
                    'root': self.root,
                    'menubutton': self.menubutton,
                    'wallpaper_label': self.wallpaper_label,
                    'x': self.x,
                    'y': self.y
                    }

            self.categorie = Categories(init)
            self.categorie.categorie()

        if self.menubutton != None:
            self.menubutton.config(command=self.hide)

        self.root.mainloop()

#-----------------------------------------------------------------------------

    def destroy_root(self, event):
        "Affiche le menu sur le bureau au niveau du clic droit"

        self.x = event.x
        self.y = event.y
        self.show = wallpaper2.Wallpaper()
        self.wallpaper_label.bind('<Button-3>', lambda event, arg1=self.x,\
            arg2=self.y, arg3=self.wallpaper_label: self.show.first_show\
            (arg1, arg2, arg3))
        self.root.destroy()


#-----------------------------------------------------------------------------

    def hide(self):
        """iconification du menu principal"""


        self.root.destroy()
        self.menubutton.config(command=self.show)

#-----------------------------------------------------------------------------

    def show(self):
        """déiconification du menu principal"""

        self.menu_principal()
        self.menubutton.config(command=self.hide)

#=============================================================================

class Categories(object) :
    "Instancie les boutons des différentes catégories"

#-----------------------------------------------------------------------------

    def __init__(self, init):
        "Constructeur"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#-----------------------------------------------------------------------------

    def categorie(self):
        "Instancie les boutons des différentes catégories"

        self.app_button = tk.Button(self.label, image=self.img2,\
            text=self.app_title, bd=0, highlightthickness=0,\
            activebackground='white', cursor='hand1', compound='top',\
            bg='white')
        self.app_button.grid(row=self.k, column=self.j, padx=15, pady=5)

        if type(self.app_command) == str and self.app_title\
            != 'Déconnexion':
            self.app_button.configure(command=self.commande)
        elif self.app_title == 'Déconnexion':
            self.app_button.configure(command=self.logout_window)
        else :
            with open(os.path.join(self.dir_name, self.app_title), 'r')\
                as ofile:
                rfile = ofile.readlines()

            init = dict()
            init =  {
                    'app_command': self.app_command,
                    'rfile': rfile,
                    'dir_app': self.dir_app,
                    'dir_name': self.dir_name,
                    'img': self.img,
                    'icon': self.icon,
                    'app_title': self.app_title,
                    'img2': self.img2,
                    'j': self.j,
                    'k': self.k,
                    'label': self.label,
                    'root': self.root,
                    'menubutton': self.menubutton,
                    'wallpaper_label': self.wallpaper_label,
                    'x': self.x,
                    'y': self.y
                    }

            self.app_command = Icones(init)
            self.app_button.configure(command=self.app_command.application)

#-----------------------------------------------------------------------------


    def commande(self):
        """Exécution de la commande sélectionnée
        et iconification du menu principal"""

        self.root.destroy()
        os.system('nohup {} & exit'.format(self.app_command))
        init = dict()
        init =  {
                'dir_name': self.dir_name,
                'root': self.root,
                'menubutton': self.menubutton,
                'wallpaper_label': self.wallpaper_label,
                'x': self.x,
                'y': self.y
                }

        if self.menubutton != None:
            self.show = MenuPrincipal(init)
            self.menubutton['command'] = self.show.show
        elif self.wallpaper_label != None:
            self.show = MenuPrincipal(init)
            self.wallpaper_label.bind('<Button-3>', lambda event, arg1=self.x,\
                arg2=self.y, arg3=self.wallpaper_label: self.show.menu_principal\
                (arg1, arg2, arg3))

#-----------------------------------------------------------------------------

    def logout_window(self):
        """Ouverture de la fenêtre de déconnexion
           et iconification du menu principal"""

        self.root.destroy()
        init = dict()
        init =  {
                'dir_name': self.dir_name,
                'root': self.root,
                'menubutton': self.menubutton,
                'wallpaper_label': self.wallpaper_label,
                'x': self.x,
                'y': self.y
                }

        if self.menubutton != None:
            self.show = MenuPrincipal(init)
            self.menubutton['command'] = self.show.show
        elif self.wallpaper_label != None:
            self.show = MenuPrincipal(init)
            self.wallpaper_label.bind('<Button-3>', lambda event, arg1=self.x,\
                arg2=self.y, arg3=self.wallpaper_label: self.show.menu_principal\
                (arg1, arg2, arg3))

        self.app_command()

#=============================================================================

class Icones(object) :
    "Création des objets des différentes applications"

#-----------------------------------------------------------------------------

    def __init__(self, init) :
        "Constructeur"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#-----------------------------------------------------------------------------

    def application(self):
        "Création des objets des différentes applications"

        for child in self.label.winfo_children():
            if child.winfo_class() == 'Labelframe' and child['bd'] > 0:
                child.grid_remove()

        self.labelframe = tk.LabelFrame(self.label, bg='white', bd=1, padx=15)
        self.labelframe.grid(row=1, column=self.j+3, rowspan=15, padx=5,\
            pady=5, sticky='n')

        list_execute, list_icone, list_name = [], [], []
        for i, item in enumerate(self.rfile):
            item = item.strip()
            if item.startswith('exec'):
                execute = item.replace('exec:', '')
                list_execute.append(execute) #commande

            elif item.startswith('icon'):
                icone = item.replace('icon:', '')
                list_icone.append(icone) #image

            elif item.startswith('name'):
                name = item.replace('name:', '')
                list_name.append(name) # nom de l'application

        for i, element in enumerate(list_execute):
            self.k+=1

            init = dict()
            init =  {
                    'app_command': self.app_command,
                    'rfile': self.rfile,
                    'dir_app': self.dir_app,
                    'dir_name': self.dir_name,
                    'img': self.img,
                    'execute': list_execute[i],
                    'app_title': self.app_title,
                    'icone': list_icone[i],
                    'name': list_name[i],
                    'j': self.j,
                    'k': self.k,
                    'label': self.label,
                    'root': self.root,
                    'labelframe': self.labelframe,
                    'menubutton': self.menubutton,
                    'wallpaper_label': self.wallpaper_label,
                    'x': self.x,
                    'y': self.y
                    }

            self.appli = Applications(init)
            self.appli.application()


#=============================================================================

class Applications(object):
    "Instancie les boutons des différentes applications"

#-----------------------------------------------------------------------------

    def __init__(self, init):
        "Constructeur"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#-----------------------------------------------------------------------------

    def application(self):
        "Instancie les boutons des différentes applications"

        icon_size=32, 32
        icon = Image.open(os.path.join(self.dir_name, 'icones', self.icone))
        icon.thumbnail(icon_size)
        self.icon_32 = ImageTk.PhotoImage(icon, master = self.root)
        self.icone = [self.icon_32]

        init = dict()
        init =  {
                'app_command': self.app_command,
                'dir_app': self.dir_app,
                'dir_name': self.dir_name,
                'execute': self.execute,
                'icone': self.icone,
                'app_title': self.app_title,
                'name': self.name,
                'labelframe': self.labelframe,
                'label': self.label,
                'root': self.root,
                'menubutton': self.menubutton,
                'wallpaper_label': self.wallpaper_label,
                'x': self.x,
                'y': self.y
                }

        self.commande=Commande(init)

        self.app_button = tk.Button(self.labelframe, image=self.icone,\
            text=self.name, bd=0, highlightthickness=0, activebackground='white',\
            cursor='hand1', compound='top', bg='white',\
            command=self.commande.commande)
        self.app_button.grid(row = self.k, column = self.j, padx=5, pady=5)

#=============================================================================

class Commande(object):
    "Lancement des commandes"

#-----------------------------------------------------------------------------

    def __init__(self, init):
        "constructeur"

        if isinstance(init, dict):
            for i in init:
                setattr(self, i, init[i])

#-----------------------------------------------------------------------------

    def commande(self):
        "Lancement des commandes"

        if self.execute == 'ajouter':
            for child in self.labelframe.winfo_children():
                child.grid_remove()

            self.frame = tk.Frame(self.labelframe, bg='white')
            self.frame.grid(row=2, column=4, rowspan=10, sticky='nsew')

            self.label_add_app = tk.Label(self.frame, bg='white',
                font='Courier 14 bold', justify='left',
                text="Sélectionnez dans la liste déroulante,\n\
l'application que vous souhaitez rajouter au menu principal.")
            self.label_add_app.grid(row=0, column=1, padx=10, pady=10)

            self.add_ok = tk.Button(self.frame, bg='white', font='Courier 14 bold',
                text='OK', command=self.show_files)
            self.add_ok.grid(row=1, column=1, padx=10, pady=5)

        elif self.execute == 'retirer' :
            self.frame = tk.Frame(self.labelframe, bg='white')
            self.frame.grid(row=2, column=4, rowspan=10, sticky='nsew')

            self.label_suppress_app = tk.Label(self.frame, bg='white',
                font='Courier 14 bold', text="Entrez le nom de l'application\n\
                que vous souhaitez retirer\ndu menu principal.")
            self.label_suppress_app.grid(row=0, column=1, padx=10, pady=10,
                sticky='nsew')

            self.suppress_app = tk.Entry(self.frame, font='Courier 14 bold', width=30)
            self.suppress_app.focus_force()
            self.suppress_app.grid(row=1, column=1,padx=10, pady=5,
                sticky='nsew')
            self.suppress_app.bind('<KeyPress-Return>', self.erase)

        else :
            os.system('nohup {} & exit'.format(self.execute))
            self.root.after(1200, self.root.destroy)
            init = dict()
            init =  {
                    'dir_name': self.dir_name,
                    'root': self.root,
                    'menubutton': self.menubutton,
                    'wallpaper_label': self.wallpaper_label,
                    'x': self.x,
                    'y': self.y
                    }

            if self.menubutton != None:
                self.show = MenuPrincipal(init)
                self.menubutton['command'] = self.show.show
            elif self.wallpaper_label != None:
                self.show = MenuPrincipal(init)
                self.wallpaper_label.bind('<Button-3>', lambda event,\
                    arg1=self.x, arg2=self.y, arg3=self.wallpaper_label:\
                    self.show.menu_principal(arg1, arg2, arg3))

#-----------------------------------------------------------------------------

    def show_files(self):
        "Sélection de l'application à rajouter"

        self.add_ok.destroy()

        self.label_add_app.configure(text='Faites votre choix',\
            font='Courier 14 bold')

        self.listbox_add_app = tk.Listbox(self.frame,
            selectbackground='white', selectborderwidth=2)
        self.listbox_add_app.grid(row=2, column=1, pady=5, padx=5)

        self.app_liste = os.listdir(self.dir_app)
        self.app_liste.sort(key=str.lower)

        for i, app in enumerate(self.app_liste):
            self.listbox_add_app.insert('end', ' {}'.format(app.replace\
                ('.desktop', '')))

        self.listbox_add_app.bind('<Double-Button-1>', self.select_app)

#-----------------------------------------------------------------------------

    def select_app(self, event):
        """Sélectionne l'application dans la liste déroulante"""

        self.commandos = []
        self.selected_app = os.path.join(self.dir_app,
            self.listbox_add_app.get('active').strip() + '.desktop')

        with open(self.selected_app, 'r') as ofile:
            rfile = ofile.readlines()
            for line in rfile :
                if line.startswith('Exec='):
                    self.commandos.append(
                    line.strip().replace('Exec=', 'exec:'))
                    break
                    
            for line2 in rfile :
                if line2.startswith('Icon'):
                    line2 = line2.split('=')
                    for item in os.listdir(os.path.join(self.dir_name, 'icones')):
                        if line2[1].strip() in item :
                            self.commandos.append('icon:' + os.path.join(self.dir_name,\
                                'icones', item))
                            break
                    else:
                        self.commandos.append('icon:' + os.path.join(self.dir_name,\
                            'icones', 'gnome-settings-default-applications.png'))

            for line3 in rfile :
                if line3.startswith('Name[fr'):
                    line3 = line3.split('=')
                    self.commandos.append('name:' + line3[1].strip())
                    break

                elif line3.startswith('Name='):
                    line3 = line3.split('=')
                    self.commandos.append('name:' + line3[1].strip())
                    break

        self.insert_icon()

#-----------------------------------------------------------------------------

    def insert_icon(self):
        """Intègre l'icône sélectionnée"""

        letters = ['%U', '%u', '%F', '%f', '-pkexec']
        for i, item in enumerate(self.commandos):
            for letter in letters :
                if letter in item :
                    if not letter =='-pkexec':
                        item = item.replace(letter,'')
                    else :
                        item = 'exec:uxterm -e \"sudo ' +\
                            item.replace(letter,'').replace('exec:', '')+ '\"'
                self.commandos[i:i+1]=[item.strip()]

        with open(os.path.join(self.dir_name, self.app_title), 'r') as ofile:
            rfile = ofile.readlines()
            for commando in self.commandos:
                rfile.insert(-6, commando + '\n')
        with open(os.path.join(self.dir_name, self.app_title) , 'w') as wfile:
            for item in rfile:
                wfile.write(item)

        for child in self.frame.winfo_children():
            if child.winfo_class() == 'Button' or \
                child.winfo_class() == 'Listbox':
                child.grid_remove()

        self.label_add_app.configure(text="L'application a été rajoutée")
        self.root.after(1000, self.root.destroy)
        init = dict()
        init =  {
                'dir_name': self.dir_name,
                'root': self.root,
                'menubutton': self.menubutton,
                'wallpaper_label': self.wallpaper_label,
                'x': self.x,
                'y': self.y
                }

        if self.menubutton != None:
            self.show = MenuPrincipal(init)
            self.menubutton['command'] = self.show.show
        elif self.wallpaper_label != None:
            self.show = MenuPrincipal(init)
            self.wallpaper_label.bind('<Button-3>', lambda event, arg1=self.x,\
                arg2=self.y, arg3=self.wallpaper_label: self.show.menu_principal\
                (arg1, arg2, arg3))

#-----------------------------------------------------------------------------

    def erase(self, event):
        """Retire une application"""

        app_to_suppress = self.suppress_app.get()

        if app_to_suppress.strip().lower() == 'ajouter' or \
            app_to_suppress.strip().lower() == 'retirer':
            tkmess.showwarning('Opération non valide',
                "Cette opération n'est pas valide", parent=self.root)
            self.suppress_app.delete(0, 30)

        else :
            with open(os.path.join(self.dir_name, self.app_title), 'r')\
                as ofile:
                rfile = ofile.readlines()

            rfile2 = list()
            for commando in rfile :
                rfile2.append(
                    commando.strip().lower().replace('name:', ''))

            if app_to_suppress.strip().lower() not in \
                rfile2:
                for child in self.frame.winfo_children():
                    if child.winfo_class() == 'Button' or \
                        child.winfo_class() == 'Entry' :
                        child.grid_remove()

                self.label_suppress_app.configure(
                    text="Cette application n'existe pas!")

                self.root.after(1000, self.root.destroy)
                init = dict()
                init =  {
                        'dir_name': self.dir_name,
                        'root': self.root,
                        'menubutton': self.menubutton,
                        'wallpaper_label': self.wallpaper_label,
                        'x': self.x,
                        'y': self.y
                        }

                if self.menubutton != None:
                    self.show = MenuPrincipal(init)
                    self.menubutton['command'] = self.show.show
                elif self.wallpaper_label != None:
                    self.show = MenuPrincipal(init)
                    self.wallpaper_label.bind('<Button-3>', lambda event,\
                        arg1=self.x, arg2=self.y, arg3=self.wallpaper_label:\
                        self.show.menu_principal(arg1, arg2, arg3))

            else :
                for i, commando in enumerate(rfile):
                    commando_lower = commando.strip().lower().replace(
                        'name:', '')
                    app_suppress_lower = app_to_suppress.strip().lower()
                    if commando_lower == app_suppress_lower and (i+1) % 3 == 0:
                        rfile[i-2:i+1] = []

                with open(os.path.join(self.dir_name, self.app_title), 'w')\
                    as wfile:
                    for commando in rfile:
                        wfile.write(commando)

                for child in self.frame.winfo_children():
                    if child.winfo_class() == 'Button' or \
                    child.winfo_class() == 'Entry' :
                        child.grid_remove()

                self.label_suppress_app.configure(
                    text="L'application a été retirée")

                self.root.after(1000, self.root.destroy)
                init = dict()
                init =  {
                        'dir_name': self.dir_name,
                        'root': self.root,
                        'menubutton': self.menubutton,
                        'wallpaper_label': self.wallpaper_label,
                        'x': self.x,
                        'y': self.y
                        }

                if self.menubutton != None:
                    self.show = MenuPrincipal(init)
                    self.menubutton['command'] = self.show.show
                elif self.wallpaper_label != None:
                    self.show = MenuPrincipal(init)
                    self.wallpaper_label.bind('<Button-3>', lambda event,\
                        arg1=self.x, arg2=self.y, arg3=self.wallpaper_label:\
                        self.show.menu_principal(arg1, arg2, arg3))
