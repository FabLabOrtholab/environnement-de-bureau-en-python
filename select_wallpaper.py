#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import tkinter as tk # Importation du module tkinter
import os
from PIL import Image, ImageTk
import sys
from tkinter.filedialog import *
from tkinter.messagebox import *
import wallpaper
import time

#==== CLASSES ET MÉTHODES ====================================================

class Images_rep(object):
    "Instanciation de la barre de tâches"

#-----------------------------------------------------------------------------

    def __init__(self):
        "Constructeur"

        self.login = os.getlogin()
        self.home = os.path.join('/home', self.login)
        self.autostart = os.path.join(self.home, '.config', 'openbox',
            'autostart')

#-----------------------------------------------------------------------------

    def images_rep(self):

        self.dir_name = os.path.dirname(sys.argv[0])
        self.root = tk.Tk()
        self.root.title('Tapisserie')
        self.label= tk.Label(self.root, bg='white')
        self.label.grid()
        #self.root.attributes('-type', 'splash')
        #self.root.geometry("%sx%s+%s-0" % (self._geometry[0], self._geometry[1], self._geometry[2]))
        self.label2 = tk.Label(self.label, bg='white', font='Courier 14 bold',
            text="Bonjour,\nVoulez-vous une image fixe en fond d'écran\n" + \
            "ou un défilement aléatoire?")
        self.label2.grid(padx=5, pady=5)
        self.label_frame_button = tk.LabelFrame(self.label, bg='white', bd=0)
        self.label_frame_button.grid(row=1, padx=5, pady=5)
        self.button_1 = tk.Button(self.label_frame_button, bg='white',
            text='Image fixe', font='Courier 14 bold', command=self.image_fixe)
        self.button_1.grid(row=0, column=0, padx=5, pady=5)
        self.button_2 = tk.Button(self.label_frame_button, bg='white',
            text='Défilement aléatoire', font='Courier 14 bold',
            command=self.defil_aleat)
        self.button_2.grid(row=0, column=1, padx=5, pady=5)
        self.root.mainloop()

    def image_fixe(self):

        self.chosen_file = askopenfilename(title='Choisissez une image',
            initialdir=self.home)

        ofile = os.path.join(self.dir_name, 'one_image')
        with open (ofile, 'w') as one_img :
            one_img.write(self.chosen_file)

        self.launcher = os.path.join(self.dir_name, 'launcher.py')
        os.system("sed -i -e 's/wallpaper/wallpaper2/g' " + self.launcher)
        showinfo('Modification prise en compte',
            'Le changement sera effectif au prochain redémarrage')
        self.root.destroy()

    def defil_aleat(self) :

        self.chosen_rep = askdirectory(title='Choisissez une image',
            initialdir=self.home)

        ofile = os.path.join(self.dir_name, 'selected_rep')
        with open (ofile, 'w') as wfile:
            wfile.write(self.chosen_rep)

        self.launcher = os.path.join(self.dir_name, 'launcher.py')
        os.system("sed -i -e 's/wallpaper2/wallpaper/g' " + self.launcher)
        self.label_frame_button.grid_remove()
        self.label2.configure(
            text="Choisissez la fréquence de rafraîchissement")
        self.scale = tk.Scale(self.label, bg='white', bd=0, orient='horizontal',
            from_=1, to=60, resolution=10, tickinterval=5, length=350,
            label='Fréquence de rafraîchessement en minutes',
            command=self.minutes)
        self.scale.grid(row=1, padx=5, pady=5)
        self.label_frame_button.grid(row=2)
        self.button_2.destroy()
        self.button_1.configure(text='Enregistrer', command=self.stock_value)
        self.button_1.grid(padx=5, pady=5)

    def minutes(self, _minutes):

        self._minutes = _minutes
        ofile = os.path.join(self.dir_name, 'minutes')
        with open(ofile, 'w') as wfile:
            if self._minutes == '0' :
                wfile.write('1')
            else :
                wfile.write(self._minutes)

    def stock_value(self) :

        showinfo('Modification prise en compte',
            'Le changement sera effectif au prochain redémarrage')
        self.root.destroy()

# == Main programm ===========================================================

if __name__ == "__main__":

    images_rep = Images_rep()
    images_rep.images_rep()
