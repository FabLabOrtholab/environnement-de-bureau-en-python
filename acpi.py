#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import os
import sys
import time
import battery
import subprocess

dir_name = os.path.dirname(sys.argv[0])

while 1 :

    bat=subprocess.Popen(['/bin/bash', '-c', 'acpi'] , stdout=subprocess.PIPE)
    with open(os.path.join(dir_name, 'battery_state'), 'w') as wfile:
        for line in bat.stdout:
            wfile.write(line.decode('UTF-8'))
    time.sleep(1)
