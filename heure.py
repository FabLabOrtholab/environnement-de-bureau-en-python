#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Copyright © 2019 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

#==== IMPORTATION DES MODULES ================================================

import os
import sys
import datetime
import time
import tkinter as tk
from PIL import Image, ImageTk
import subprocess
import calendrier


class Heure(object):

    def __init__(self):
        "constructeur"

        print(sys.argv)

    def horloge(self):

        self.dir_name = os.path.dirname(sys.argv[0])

        screens = [[], []]
        for l in subprocess.check_output(["xrandr"]).decode("utf-8").splitlines():
            if " connected " in l :
                screens[0].append(l.split()[0])

            elif '*' in l :
                screens[1].append(l.split()[0])

        # screens = [['HDMI-O', 'VGA-0'], ['1920x1080', '1920x1080']]

        self.root = tk.Tk()
        self.root.overrideredirect(1)
        self.root.geometry('+{}-{}'.format(int(screens[1][0].split('x')[0]) - 280, 0))
        self.frame = tk.Frame(self.root, bd=0, highlightthickness=0, bg='black')
        self.frame.grid()
        print(self.root.winfo_reqwidth())


        icon_size=20, 20
        icon = Image.open(os.path.join(self.dir_name, 'icones', 'wicd.png'))
        icon.thumbnail(icon_size)
        icon_24 = ImageTk.PhotoImage(icon, master=self.root)

        self.wicd_button = tk.Button(self.frame, bg='black', bd=0, highlightthickness=0, image=icon_24, command=lambda:os.system('wicd-gtk --no-tray &'))
        self.wicd_button.grid(row=0, column=0, padx=5)

        self.date_button = tk.Button(self.frame, bg='black', activebackground='green',\
        activeforeground='white', fg='white', font='Courier 14 bold', bd=0,\
        highlightthickness=0, command=self.show)

        while 1 :
            self.root.after(1000, self.heure)
            self.root.update()

    def heure(self):

        date = datetime.datetime.now()
        day_ = '0{}'.format(str(date.day)) if date.day < 10 else '{}'.format(str(date.day))
        month_ = '0{}'.format(str(date.month)) if date.month < 10 else '{}'.format(str(date.month))
        hour_ = '0{}'.format(str(date.hour)) if date.hour < 10 else '{}'.format(str(date.hour))
        minute_ = '0{}'.format(str(date.minute)) if date.minute < 10 else '{}'.format(str(date.minute))
        second_ = '0{}'.format(str(date.second)) if date.second < 10 else '{}'.format(str(date.second))
        self.date_ = '{}/{}/{}  {}:{}:{}'.format(day_, month_, date.year, hour_, minute_, second_)
        self.date_button.config(text=self.date_)
        self.date_button.grid(row=0, column=1, padx=5, pady=2)

    def show(self) :

        self.date_button.config(command=self.withdraw_)
        self.app = subprocess.Popen(['/bin/bash', '-c', os.path.join(self.dir_name, 'calendrier.py')])

    def withdraw_(self):

        os.system("kill {}".format(self.app.pid))
        self.date_button.config(command=self.show)




if __name__ == "__main__":

        heure = Heure()
        heure.horloge()

